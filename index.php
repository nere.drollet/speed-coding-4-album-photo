<?php
    $uploaded = (isset($_GET["uploaded"]) && $_GET["uploaded"] == "true") ? true : false;
    $error = (isset($_GET["error"]) && $_GET["error"] == "true") ? true : false;
    $errorType = $_GET["errorType"];
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>training</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <header>
    </header>
    <?php if($uploaded){ ?>
    <div class="alert alert-success" role="alert">
        Fichier bien uploadé
    </div>
    <?php } ?>
    <?php if($error){ ?>
    <div class="alert alert-danger" role="alert">
        <?php if($errorType == "size") { ?>Votre fichier est trop lourd<?php } ?>
        <?php if($errorType == "type") { ?>Nous n'acceptons que les images<?php } ?>
    </div>
    <?php } ?>
    <div class="container mt-5">
        <form action="upload.php" method="POST" enctype="multipart/form-data">
            <div class="custom-file">
                <input type="file" name="fichier">
            </div>
            <button type="submit" class="btn btn-success mt-3 btn-block">VALIDER</button>
        </form>
      <a href="album.php">ALBUM</a>
    </div>
    <footer></footer>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body>

</html>