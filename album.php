<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>training</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <header></header>
    <?php
$urlphoto = "/Users/tatia/OneDrive/Bureau/new_folder/upload/";

// nom du répertoire qui contient les images
$nomRepertoire = "/Users/tatia/OneDrive/Bureau/new_folder/upload/";
if (is_dir($nomRepertoire))
   {
   $dossier = opendir($nomRepertoire);
   while ($Fichier = readdir($dossier))
       {
      if ($Fichier != "." AND $Fichier != ".." AND (stristr($Fichier,'.gif') OR stristr($Fichier,'.jpg') OR stristr($Fichier,'.png') OR stristr($Fichier,'.bmp')))
        {
        // Hauteur de toutes les images
        $h_vign = "120";
        $taille = getimagesize($nomRepertoire."/".$Fichier);
        $reduc  = floor(($h_vign*100)/($taille[1]));
        $l_vign = floor(($taille[0]*$reduc)/100);
      
          echo '<a target="_blank" href="', $urlphoto, '/',$Fichier, '">';
          echo '<img src="', $urlphoto, '/',$Fichier, '" ';
          echo "width='$l_vign' height='$h_vign'>";
          echo "</a>&nbsp;";
          }
        }    
   closedir($dossier);
   }else{
   echo' Le répertoire spécifié n\'existe pas';
   }
?>
    <footer></footer>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body>

</html>