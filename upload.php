<?php

    include "functions.php";
    $sizeAccepted = 10000000;

    $valid = false;
    if($_FILES["fichier"]["size"] > $sizeAccepted){
        header("location: index.php?error=true&errorType=size");
    } else {
        if($_FILES["fichier"]["type"] != "image/jpg" && $_FILES["fichier"]["type"] != "image/jpeg" && $_FILES["fichier"]["type"] != "image/png"){
            header("location: index.php?error=true&errorType=type");
        }else{
            $valid = true;
        }
    }


    if($valid){
        $uploaddir = '/Users/tatia/OneDrive/Bureau/new_folder/upload/';
        $uploadfile = $uploaddir . basename($_FILES['fichier']['name']);

        echo '<pre>';
        if (move_uploaded_file($_FILES['fichier']['tmp_name'], $uploadfile)) {
            header("location: index.php?uploaded=true");
        } else {
            echo "Attaque potentielle par téléchargement de fichiers";
        }
    }
?>